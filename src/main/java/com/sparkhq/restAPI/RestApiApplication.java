package com.sparkhq.restAPI;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.sparkhq.restAPI.entities.Note;
import com.sparkhq.restAPI.repositories.NoteRepository;

@ComponentScan({"com.sparkhq.restAPI.controller"})
@ComponentScan({"com.sparkhq.restAPI.services"})
@EntityScan("com.sparkhq.restAPI.entities")
@EnableJpaRepositories("com.sparkhq.restAPI.repositories")
@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
	}
	
	@Bean
    CommandLineRunner initDatabase(NoteRepository repository) {
        return args -> {
            repository.save(new Note("Note 1"));
            repository.save(new Note("Note 2"));
            repository.save(new Note("Note 3"));
        };
    }
}
