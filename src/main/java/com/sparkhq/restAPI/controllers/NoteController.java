package com.sparkhq.restAPI.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sparkhq.restAPI.entities.Note;
import com.sparkhq.restAPI.services.NoteService;

@RestController
public class NoteController {
	
	@Autowired
	private final NoteService service;

  	public NoteController(NoteService service) {
	  	this.service = service;
  	}

  	@GetMapping("/note")
  	public List<Note> getAllNotes() {
	  	return service.findAll();
  	}

  	@PostMapping("/note")
  	public String createNote(@RequestBody String content) {
	  	return service.save(new Note(content));
  	}
  
  	@GetMapping("/note/{id}")
  	public String getNoteByID(@PathVariable String id) {    
  		return service.findById(id);
    }

  	@DeleteMapping("/note/{id}")
  	public void deleteNoteByID(@PathVariable String id) {
  		service.deleteById(id);
  	}
}