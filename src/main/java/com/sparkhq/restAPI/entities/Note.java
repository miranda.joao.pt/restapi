package com.sparkhq.restAPI.entities;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Note {

	private @Id String id;
	private @DateTimeFormat String created;
	private String content;


	public Note() {
		this.id = UUID.randomUUID().toString();
		this.created = getUTCDateTimeAsString();
	}
	
	public Note(String content) {
		this.id = UUID.randomUUID().toString();
		this.created = getUTCDateTimeAsString();
		this.content = content;
	}

	public String getId() {
		return this.id;
	}

	public String getCreated() {
		return this.created;
	}

	public String getContent() {
		return this.content;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public boolean equals(Object o) {
	    if (this == o)
	    	return true;
	    if (!(o instanceof Note))
	    	return false;
	    Note note = (Note) o;
	    return Objects.equals(this.id, note.id) 
	    		&& Objects.equals(this.created, note.created) 
	    		&& Objects.equals(this.content, note.content);
	  }

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.created, this.content);
	}

	@Override
	public String toString() {
		return "Note{" + "id=" + this.id + ", created='" + this.created + '\'' + ", content='" + this.content + '\'' + '}';
	}
	
	private static String getUTCDateTimeAsString() {
	    final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	    final String utcTime = dateFormat.format(new Date());

	    return utcTime;
	}
}