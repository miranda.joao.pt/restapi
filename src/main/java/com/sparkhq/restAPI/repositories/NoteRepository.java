package com.sparkhq.restAPI.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.sparkhq.restAPI.entities.Note;

@Service
public interface NoteRepository extends JpaRepository<Note, String> {
	

}
