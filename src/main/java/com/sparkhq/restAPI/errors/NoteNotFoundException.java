package com.sparkhq.restAPI.errors;

public class NoteNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoteNotFoundException(String id) {
		super("Could not find note with id: " + id);
	}
}