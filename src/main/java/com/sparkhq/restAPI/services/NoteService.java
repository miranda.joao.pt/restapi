package com.sparkhq.restAPI.services;

import java.util.List;

import com.sparkhq.restAPI.entities.Note;

public interface NoteService {
	public List<Note> findAll();
    public String save(Note note);
    public String findById(String id);
    public void deleteById(String id);
}
