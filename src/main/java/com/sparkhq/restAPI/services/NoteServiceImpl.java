package com.sparkhq.restAPI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.sparkhq.restAPI.entities.Note;
import com.sparkhq.restAPI.errors.NoteNotFoundException;
import com.sparkhq.restAPI.repositories.NoteRepository;

@Component
@Service
public class NoteServiceImpl implements NoteService {
	@Autowired
    private NoteRepository repository;
	
	public NoteServiceImpl() {
		
	}

    @Override
    public List<Note> findAll() {
        List<Note> listUsers = repository.findAll();
        return listUsers;
    }

    @Override
    public String save(Note note) {
    	Note savedNote = repository.save(note);
	  	return savedNote.getId();
    }
    
    @Override
    public String findById(String id) {
    	Note note = repository.findById(id)
  				.orElseThrow(() -> new NoteNotFoundException(id));
    
  		return note.getContent();
    }

    @Override
    public void deleteById(String id) {
    	repository.deleteById(id);
    }
}
