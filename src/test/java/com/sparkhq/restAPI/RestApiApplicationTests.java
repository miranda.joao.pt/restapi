package com.sparkhq.restAPI;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sparkhq.restAPI.controllers.NoteController;
import com.sparkhq.restAPI.entities.Note;

@SpringBootTest
class RestApiApplicationTests {
	
    @Autowired
    private NoteController noteController;

   
    @Test
	private void test_00_getAllNotes() {	   
	   List<Note> notes = noteController.getAllNotes();
	   
	   assertNotNull(notes);
	}
    
    @Test
    private void test_01_createNote() {	   
	   String newNoteId = noteController.createNote("Test Note");
	   
	   assertNotNull(newNoteId);
	}
    
    @Test
    private void test_02_getNoteByID() {	 
 	   	String noteContent = noteController.getNoteByID(noteController.getAllNotes().get(0).getId());

	   	assertNotNull(noteContent);
	}

    @Test
    private void test_03_deleteNoteByID() {	 
    	List<Note> notesBeforeDeletion = noteController.getAllNotes();
    	int numberOfNotesBeforeDeletion = notesBeforeDeletion.size();
    	Note firstNote = notesBeforeDeletion.get(0);

    	noteController.deleteNoteByID(firstNote.getId());
   	   
   	   	List<Note> notesAfterDeletion = noteController.getAllNotes();
    	int numberOfNotesAfterDeletion = notesAfterDeletion.size();

   	   	assertEquals(numberOfNotesBeforeDeletion, numberOfNotesAfterDeletion + 1);
   	}
}
