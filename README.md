# Read Me First

## Start API on localhost:8080
mvn spring-boot:run

## Run unit tests
mvn test

## Endpoints
### GET /note/: return all the notes
curl -v localhost:8080/note

### POST /note: create a new note and return the note ID
curl -X POST localhost:8080/note -H 'Content-type:application/json' -d 'Test Content'

### GET /note/[note uuid]: return the note payload
curl -v localhost:8080/note/[note uuid]

### DELETE /note/[note uuid]: delete the specified note
curl -X DELETE -v localhost:8080/note/[note uuid]
